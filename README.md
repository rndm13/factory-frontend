# A web client for a "factory" api
Made for a school assignment

## Notable Features
- A nested table
- Select with pagination fetching (using react-select)

## Installation 
```
git clone https://gitlab.com/rndm13/factory-frontend.git 
cd factory-frontend
npm i
npm run dev
```
