import { PaginationControls } from "@/components/ui/pagination-controls";
import { Button } from "@/components/ui/button";
import {
  CardTitle,
  CardDescription,
  CardHeader,
  CardContent,
  Card,
} from "@/components/ui/card";
import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";
import {
  TableHead,
  TableRow,
  TableHeader,
  TableCell,
  TableBody,
  Table,
} from "@/components/ui/table";
import { FileEditIcon, TrashIcon } from "@/components/ui/icons";

import { useState } from "react";
import { useQuery, useMutation, useQueryClient } from "@tanstack/react-query";

import {
  Dialog,
  DialogTrigger,
  DialogContent,
  DialogTitle,
  DialogHeader,
  DialogClose,
  DialogFooter,
} from "@/components/ui/dialog";
import {
  Collapsible,
  CollapsibleContent,
  CollapsibleTrigger,
} from "@/components/ui/collapsible";
import axios from "axios";
import { DeletionModal } from "@/components/ui/deletion-modal";
import { SearchBar } from "./components/ui/search-bar";
import { Customer, CustomerOption } from "./customers";
import { Product, ProductOption } from "./products";
import AsyncSelect from "react-select/async";
import { SheetIcon } from "lucide-react";

export interface OrderProduct {
  id: number;
  count: number;
  order_id: number;
  order?: Order;
  product_id: number;
  product?: Product;
  created_at: Date;
  updated_at: Date;
}

export interface Order {
  id: number;
  progress: string;
  customer_id: number;
  customer?: Customer; // only available in get requests
  products?: OrderProduct[]; // only available in get requests
  created_at: Date;
  updated_at: Date;
}

export function OrderModal({
  title,
  originalOrder,
  mutation,
}: {
  title: string;
  originalOrder: Order;
  mutation: any;
}) {
  const [order, setOrder] = useState<Order>(originalOrder);
  const filterCustomers = (search: string, options: CustomerOption[]) => {
    return options.filter(
      (opt) =>
        opt.customer.first_name.toLowerCase().includes(search.toLowerCase()) ||
        opt.customer.last_name.toLowerCase().includes(search.toLowerCase()),
    );
  };
  const customerOptions = async (search: string) => {
    const res = await fetch(
      `http://127.0.0.1:8000/api/customers?search=${search}`,
    );
    const json = await res.json();
    const options = json.data.map((customer: Customer) => {
      const cust = new CustomerOption(customer);
      console.log(cust.value);
      console.log(cust.label);
      return cust;
    });
    console.log(options);
    const filtered = filterCustomers(search, options);
    console.log(filtered);
    return filtered;
  };

  return (
    <DialogContent>
      <DialogHeader>
        <DialogTitle>{title}</DialogTitle>
      </DialogHeader>
      {mutation.isPending ? (
        <p>Processing...</p>
      ) : mutation.isError ? (
        <>
          <div>An error occurred: {mutation.error.message}</div>
          <DialogFooter>
            <DialogClose asChild>
              <Button variant="outline" onClick={() => mutation.reset()}>
                Close
              </Button>
            </DialogClose>
          </DialogFooter>
        </>
      ) : mutation.isSuccess ? (
        <>
          <div>Successfully saved!</div>
          <DialogFooter>
            <DialogClose asChild>
              <Button variant="outline" onClick={() => mutation.reset()}>
                Close
              </Button>
            </DialogClose>
          </DialogFooter>
        </>
      ) : (
        <>
          <div className="space-y-2">
            <Label htmlFor="progress">Progress</Label>
            <Input
              id="progress"
              placeholder="Cart"
              required
              value={order.progress}
              onChange={(value) =>
                setOrder((old) => {
                  return { ...old, progress: value.target.value };
                })
              }
            />
          </div>
          <div className="space-y-2">
            <Label htmlFor="customer">Customer</Label>
            <AsyncSelect
              required
              cacheOptions
              loadOptions={customerOptions}
              defaultOptions
              value={
                order.customer ? new CustomerOption(order.customer) : undefined
              }
              onChange={(new_value) =>
                setOrder((old: Order) => {
                  return {
                    ...old,
                    customer_id:
                      new_value == null
                        ? old.customer_id
                        : new_value.customer.id,
                  };
                })
              }
            />
          </div>
          <DialogFooter>
            <DialogClose asChild>
              <Button variant="outline">Cancel</Button>
            </DialogClose>
            <Button onClick={() => mutation.mutate(order)}>Save</Button>
          </DialogFooter>
        </>
      )}
    </DialogContent>
  );
}

export function OrderProductModal({
  title,
  originalOrderProduct,
  mutation,
}: {
  title: string;
  originalOrderProduct: OrderProduct;
  mutation: any;
}) {
  const [order, setOrder] = useState<OrderProduct>(originalOrderProduct);
  const filterProducts = (search: string, options: ProductOption[]) => {
    return options.filter(
      (opt) =>
        opt.product.model.toLowerCase().includes(search.toLowerCase()) ||
        opt.product.category.toLowerCase().includes(search.toLowerCase()),
    );
  };
  const productOptions = async (search: string) => {
    const res = await fetch(
      `http://127.0.0.1:8000/api/products?search=${search}`,
    );
    const json = await res.json();
    const options = json.data.map((product: Product) => {
      const cust = new ProductOption(product);
      console.log(cust.value);
      console.log(cust.label);
      return cust;
    });
    console.log(options);
    const filtered = filterProducts(search, options);
    console.log(filtered);
    return filtered;
  };

  return (
    <DialogContent>
      <DialogHeader>
        <DialogTitle>{title}</DialogTitle>
      </DialogHeader>
      {mutation.isPending ? (
        <p>Processing...</p>
      ) : mutation.isError ? (
        <>
          <div>An error occurred: {mutation.error.message}</div>
          <DialogFooter>
            <DialogClose asChild>
              <Button variant="outline" onClick={() => mutation.reset()}>
                Close
              </Button>
            </DialogClose>
          </DialogFooter>
        </>
      ) : mutation.isSuccess ? (
        <>
          <div>Successfully saved!</div>
          <DialogFooter>
            <DialogClose asChild>
              <Button variant="outline" onClick={() => mutation.reset()}>
                Close
              </Button>
            </DialogClose>
          </DialogFooter>
        </>
      ) : (
        <>
          <div className="space-y-2">
            <Label htmlFor="count">Count</Label>
            <Input
              id="count"
              type="number"
              required
              value={order.count}
              onChange={(value) =>
                setOrder((old) => {
                  return { ...old, count: +value.target.value };
                })
              }
            />
          </div>
          <div className="space-y-2">
            <Label htmlFor="product">Product</Label>
            <AsyncSelect
              required
              cacheOptions
              loadOptions={productOptions}
              defaultOptions
              value={
                order.product ? new ProductOption(order.product) : undefined
              }
              onChange={(new_value) =>
                setOrder((old: OrderProduct) => {
                  return {
                    ...old,
                    product_id:
                      new_value == null ? old.product_id : new_value.product.id,
                  };
                })
              }
            />
          </div>
          <DialogFooter>
            <DialogClose asChild>
              <Button variant="outline">Cancel</Button>
            </DialogClose>
            <Button onClick={() => mutation.mutate(order)}>Save</Button>
          </DialogFooter>
        </>
      )}
    </DialogContent>
  );
}

function OrderProductRow({
  product,
  client,
}: {
  product: OrderProduct;
  client: any;
}) {
  const updateOrderProduct = useMutation({
    mutationFn: (product: OrderProduct) => {
      return axios.put(
        "http://127.0.0.1:8000/api/order/product/" + product.id,
        product,
      );
    },
    onSuccess: () => {
      client.invalidateQueries({ queryKey: ["orders"] });
    },
  });
  const deleteOrderProduct = useMutation({
    mutationFn: () => {
      return axios.delete(
        "http://127.0.0.1:8000/api/order/product/" + product.id,
      );
    },
    onSuccess: () => {
      client.invalidateQueries({ queryKey: ["orders"] });
    },
  });

  return (
    <TableRow>
      <TableCell className="font-medium"></TableCell>
      <TableCell className="font-medium">{product.count}</TableCell>
      <TableCell className="font-medium">{product.product?.model}</TableCell>
      <TableCell className="font-medium">{product.product?.category}</TableCell>
      <TableCell className="font-medium">{product.product?.price}</TableCell>
      <TableCell className="text-right">
        <Dialog>
          <DialogTrigger asChild>
            <Button className="h-full" size="icon" variant="ghost">
              <FileEditIcon className="h-4 w-4" />
              <span className="sr-only">Edit</span>
            </Button>
          </DialogTrigger>
          <OrderProductModal
            title="Update Order Product Record"
            mutation={updateOrderProduct}
            originalOrderProduct={product}
          />
        </Dialog>
        <Dialog>
          <DialogTrigger asChild>
            <Button className="h-full" size="icon" variant="ghost">
              <TrashIcon className="h-4 w-4" />
              <span className="sr-only">Delete</span>
            </Button>
          </DialogTrigger>
          <DeletionModal mutation={deleteOrderProduct} />
        </Dialog>
      </TableCell>
    </TableRow>
  );
}

function OrderProducts({ order, client }: { order: Order; client: any }) {
  const addOrder = useMutation({
    mutationFn: (newOrder: Order) => {
      return axios.post("http://127.0.0.1:8000/api/order/product", newOrder);
    },
    onSuccess: () => {
      client.invalidateQueries({ queryKey: ["orders"] });
    },
  });

  return (
    <>
      <TableRow>
        <TableCell></TableCell>
        <TableCell>
          <Dialog>
            <DialogTrigger asChild>
              <Button size="sm">Add order</Button>
            </DialogTrigger>
            <OrderProductModal
              title="Add Order"
              originalOrderProduct={{
                id: 0,
                count: 0,
                product_id: 0,
                order_id: order.id,
                created_at: new Date(),
                updated_at: new Date(),
              }}
              mutation={addOrder}
            />
          </Dialog>
        </TableCell>
      </TableRow>
      {order.products?.map((order_product) => (
        <OrderProductRow
          product={order_product}
          client={client}
          key={order_product.id}
        />
      ))}
    </>
  );
}

function OrderRow({ order, client }: { order: Order; client: any }) {
  const updateOrder = useMutation({
    mutationFn: (order: Order) => {
      return axios.put("http://127.0.0.1:8000/api/order/" + order.id, order);
    },
    onSuccess: () => {
      client.invalidateQueries({ queryKey: ["orders"] });
    },
  });
  const deleteOrder = useMutation({
    mutationFn: () => {
      return axios.delete("http://127.0.0.1:8000/api/order/" + order.id);
    },
    onSuccess: () => {
      client.invalidateQueries({ queryKey: ["orders"] });
    },
  });

  return (
    <Collapsible asChild>
      <>
        <TableRow>
          <TableCell className="font-medium">{order.id}</TableCell>
          <TableCell className="font-medium">{order.progress}</TableCell>
          <TableCell className="font-medium">{order.customer?.first_name}</TableCell>
          <TableCell className="font-medium">{order.customer?.last_name}</TableCell>
          <TableCell className="font-medium"></TableCell>
          <TableCell className="text-right">
            <CollapsibleTrigger asChild>
              <Button className="h-full" size="icon" variant="ghost">
                <SheetIcon className="h-4 w-4" />
                <span className="sr-only">Products</span>
              </Button>
            </CollapsibleTrigger>
            <Dialog>
              <DialogTrigger asChild>
                <Button className="h-full" size="icon" variant="ghost">
                  <FileEditIcon className="h-4 w-4" />
                  <span className="sr-only">Edit</span>
                </Button>
              </DialogTrigger>
              <OrderModal
                title="Update Order Record"
                mutation={updateOrder}
                originalOrder={order}
              />
            </Dialog>
            <Dialog>
              <DialogTrigger asChild>
                <Button className="h-full" size="icon" variant="ghost">
                  <TrashIcon className="h-4 w-4" />
                  <span className="sr-only">Delete</span>
                </Button>
              </DialogTrigger>
              <DeletionModal mutation={deleteOrder} />
            </Dialog>
          </TableCell>
        </TableRow>
        <CollapsibleContent asChild>
          <OrderProducts order={order} client={client} />
        </CollapsibleContent>
      </>
    </Collapsible>
  );
}

export function Orders() {
  const [page, setPage] = useState(1);
  const [search, setSearch] = useState("");

  const queryClient = useQueryClient();

  const { isLoading, isError, error, data } = useQuery({
    queryKey: ["orders", page, search],
    queryFn: async () => {
      const res = await fetch(
        `http://127.0.0.1:8000/api/orders?page=${page}&search=${search}`,
      );
      return await res.json();
    },
  });

  const addOrder = useMutation({
    mutationFn: (newOrder: Order) => {
      return axios.post("http://127.0.0.1:8000/api/order", newOrder);
    },
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ["orders"] });
    },
  });

  return (
    <div className="flex-1 min-h-0 w-full flex items-center justify-center p-4">
      <div className="grid w-full gap-4">
        <Card>
          <CardHeader>
            <CardTitle>Orders</CardTitle>
            <CardDescription>Manage your orders</CardDescription>
          </CardHeader>
          <CardContent>
            {isLoading ? (
              <h1 className="w-full text-center">Loading...</h1>
            ) : isError ? (
              <h1 className="w-full text-center">Error: {error.message}</h1>
            ) : (
              <div className="grid w-full gap-4">
                <div className="columns-3 items-center gap-4">
                  <Dialog>
                    <DialogTrigger asChild>
                      <Button size="sm">Add order</Button>
                    </DialogTrigger>
                    <OrderModal
                      title="Add Order"
                      originalOrder={{
                        id: 0,
                        customer_id: 0,
                        progress: "",
                        created_at: new Date(),
                        updated_at: new Date(),
                      }}
                      mutation={addOrder}
                    />
                  </Dialog>
                  <PaginationControls
                    page={page}
                    setPage={setPage}
                    lastPage={data.meta.last_page}
                  />

                  <div className="ml-auto flex items-center gap-2">
                    <SearchBar initialSearch={search} setKey={setSearch} />
                  </div>
                </div>
                <Table>
                  <TableHeader>
                    <TableRow>
                      <TableHead className="w-[100px]">Order ID</TableHead>
                      <TableHead>Progress</TableHead>
                      <TableHead>Customer</TableHead>
                      <TableHead></TableHead>
                      <TableHead></TableHead>
                      <TableHead className="w-[150px] text-center">
                        Actions
                      </TableHead>
                    </TableRow>
                  </TableHeader>
                  <TableBody>
                    {data.data.map((order: Order) => (
                      <OrderRow
                        client={queryClient}
                        order={order}
                        key={order.id}
                      />
                    ))}
                  </TableBody>
                </Table>
              </div>
            )}
          </CardContent>
        </Card>
      </div>
    </div>
  );
}
