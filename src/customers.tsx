import { PaginationControls } from "@/components/ui/pagination-controls";
import { Button } from "@/components/ui/button";
import {
  CardTitle,
  CardDescription,
  CardHeader,
  CardContent,
  Card,
} from "@/components/ui/card";
import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";
import {
  TableHead,
  TableRow,
  TableHeader,
  TableCell,
  TableBody,
  Table,
} from "@/components/ui/table";
import { FileEditIcon, TrashIcon } from "@/components/ui/icons";

import { useState } from "react";
import { useQuery, useMutation, useQueryClient } from "@tanstack/react-query";

import {
  Dialog,
  DialogTrigger,
  DialogContent,
  DialogTitle,
  DialogHeader,
  DialogClose,
  DialogFooter,
} from "@/components/ui/dialog";
import axios from "axios";
import { DeletionModal } from "@/components/ui/deletion-modal";
import { SearchBar } from "./components/ui/search-bar";

export interface Customer {
  id: number;
  first_name: string;
  last_name: string;
  address: string;
  phone_number: string;
  created_at: Date;
  updated_at: Date;
}

export class CustomerOption {
  constructor(public customer: Customer) {}

  get value(): string {
    return `${this.customer.id}`;
  }
  get label(): string {
    return `${this.customer.first_name} ${this.customer.last_name}`;
  }
  get color(): string {
    return "#ffffff";
  }
}

export function CustomerModal({
  title,
  originalCustomer,
  mutation,
}: {
  title: string;
  originalCustomer: Customer;
  mutation: any;
}) {
  const [customer, setCustomer] = useState<Customer>(originalCustomer);
  return (
    <DialogContent>
      <DialogHeader>
        <DialogTitle>{title}</DialogTitle>
      </DialogHeader>
      {mutation.isPending ? (
        <p>Processing...</p>
      ) : mutation.isError ? (
        <>
          <div>An error occurred: {mutation.error.message}</div>
          <DialogFooter>
            <DialogClose asChild>
              <Button variant="outline" onClick={() => mutation.reset()}>
                Close
              </Button>
            </DialogClose>
          </DialogFooter>
        </>
      ) : mutation.isSuccess ? (
        <>
          <div>Successfully saved!</div>
          <DialogFooter>
            <DialogClose asChild>
              <Button variant="outline" onClick={() => mutation.reset()}>
                Close
              </Button>
            </DialogClose>
          </DialogFooter>
        </>
      ) : (
        <>
          <div className="grid grid-cols-2 gap-4">
            <div className="space-y-2">
              <Label htmlFor="first-name">First name</Label>
              <Input
                id="first-name"
                placeholder="Lee"
                required
                value={customer.first_name}
                onChange={(value) =>
                  setCustomer((old) => {
                    return { ...old, first_name: value.target.value };
                  })
                }
              />
            </div>
            <div className="space-y-2">
              <Label htmlFor="last-name">Last name</Label>
              <Input
                id="last-name"
                placeholder="Robinson"
                required
                value={customer.last_name}
                onChange={(value) =>
                  setCustomer((old) => {
                    return { ...old, last_name: value.target.value };
                  })
                }
              />
            </div>
          </div>
          <div className="space-y-2">
            <Label htmlFor="phone">Phone</Label>
            <Input
              id="phone"
              placeholder="1-(555)-555-5555"
              required
              value={customer.phone_number}
              onChange={(value) =>
                setCustomer((old) => {
                  return { ...old, phone_number: value.target.value };
                })
              }
            />
          </div>
          <div className="space-y-2">
            <Label htmlFor="address">Address</Label>
            <Input
              id="address"
              placeholder="Brodway Street 74"
              required
              value={customer.address}
              onChange={(value) =>
                setCustomer((old) => {
                  return { ...old, address: value.target.value };
                })
              }
            />
          </div>
          <DialogFooter>
            <DialogClose asChild>
              <Button variant="outline">Cancel</Button>
            </DialogClose>
            <Button onClick={() => mutation.mutate(customer)}>Save</Button>
          </DialogFooter>
        </>
      )}
    </DialogContent>
  );
}

function CustomerRow({
  customer,
  client,
}: {
  customer: Customer;
  client: any;
}) {
  const updateCustomer = useMutation({
    mutationFn: (customer: Customer) => {
      return axios.put(
        "http://127.0.0.1:8000/api/customer/" + customer.id,
        customer,
      );
    },
    onSuccess: () => {
      client.invalidateQueries({ queryKey: ["customers"] });
    },
  });
  const deleteCustomer = useMutation({
    mutationFn: () => {
      return axios.delete("http://127.0.0.1:8000/api/customer/" + customer.id);
    },
    onSuccess: () => {
      client.invalidateQueries({ queryKey: ["customers"] });
    },
  });

  return (
    <TableRow>
      <TableCell className="font-medium">{customer.id}</TableCell>
      <TableCell className="font-medium">{customer.first_name}</TableCell>
      <TableCell className="font-medium">{customer.last_name}</TableCell>
      <TableCell>{customer.phone_number}</TableCell>
      <TableCell>{customer.address}</TableCell>
      <TableCell className="text-right">
        <Dialog>
          <DialogTrigger asChild>
            <Button className="h-full" size="icon" variant="ghost">
              <FileEditIcon className="h-4 w-4" />
              <span className="sr-only">Edit</span>
            </Button>
          </DialogTrigger>
          <CustomerModal
            title="Update Customer Record"
            mutation={updateCustomer}
            originalCustomer={customer}
          />
        </Dialog>
        <Dialog>
          <DialogTrigger asChild>
            <Button className="h-full" size="icon" variant="ghost">
              <TrashIcon className="h-4 w-4" />
              <span className="sr-only">Delete</span>
            </Button>
          </DialogTrigger>
          <DeletionModal mutation={deleteCustomer} />
        </Dialog>
      </TableCell>
    </TableRow>
  );
}

export function Customers() {
  const [page, setPage] = useState(1);
  const [search, setSearch] = useState("");

  const queryClient = useQueryClient();

  const { isLoading, isError, error, data } = useQuery({
    queryKey: ["customers", page, search],
    queryFn: async () => {
      const res = await fetch(
        `http://127.0.0.1:8000/api/customers?page=${page}&search=${search}`,
      );
      return await res.json();
    },
  });

  const addCustomer = useMutation({
    mutationFn: (newCustomer: Customer) => {
      return axios.post("http://127.0.0.1:8000/api/customer", newCustomer);
    },
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ["customers"] });
    },
  });

  return (
    <div className="flex-1 min-h-0 w-full flex items-center justify-center p-4">
      <div className="grid w-full gap-4">
        <Card>
          <CardHeader>
            <CardTitle>Customers</CardTitle>
            <CardDescription>Manage your customers</CardDescription>
          </CardHeader>
          <CardContent>
            {isLoading ? (
              <h1 className="w-full text-center">Loading...</h1>
            ) : isError ? (
              <h1 className="w-full text-center">Error: {error.message}</h1>
            ) : (
              <div className="grid w-full gap-4">
                <div className="columns-3 items-center gap-4">
                  <Dialog>
                    <DialogTrigger asChild>
                      <Button size="sm">Add customer</Button>
                    </DialogTrigger>
                    <CustomerModal
                      title="Add Customer"
                      originalCustomer={{
                        id: 0,
                        first_name: "",
                        last_name: "",
                        phone_number: "",
                        address: "",
                        created_at: new Date(),
                        updated_at: new Date(),
                      }}
                      mutation={addCustomer}
                    />
                  </Dialog>
                  <PaginationControls
                    page={page}
                    setPage={setPage}
                    lastPage={data.meta.last_page}
                  />

                  <div className="ml-auto flex items-center gap-2">
                    <SearchBar initialSearch={search} setKey={setSearch} />
                  </div>
                </div>
                <Table>
                  <TableHeader>
                    <TableRow>
                      <TableHead className="w-[100px]">Customer ID</TableHead>
                      <TableHead>First Name</TableHead>
                      <TableHead>Last Name</TableHead>
                      <TableHead>Phone</TableHead>
                      <TableHead>Address</TableHead>
                      <TableHead className="w-[100px] text-center">
                        Actions
                      </TableHead>
                    </TableRow>
                  </TableHeader>
                  <TableBody>
                    {data.data.map((customer: Customer) => (
                      <CustomerRow
                        client={queryClient}
                        customer={customer}
                        key={customer.id}
                      />
                    ))}
                  </TableBody>
                </Table>
              </div>
            )}
          </CardContent>
        </Card>
      </div>
    </div>
  );
}
