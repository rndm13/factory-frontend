import {
  Pagination,
  PaginationItem,
  PaginationLink,
  PaginationNext,
  PaginationContent,
  PaginationEllipsis,
  PaginationPrevious,
} from "@/components/ui/pagination";

export function PaginationControls({
  page,
  setPage,
  lastPage,
}: {
  page: number;
  setPage: any;
  lastPage: number;
}) {
  var pagination_range = 2;
  var pagination_pages = [];
  var max_page_in_range = Math.min(lastPage, page + pagination_range);

  const PageLink = (i: number) => (
    <PaginationItem key={i}>
      <PaginationLink onClick={() => setPage(i)} isActive={page == i}>
        {i}
      </PaginationLink>
    </PaginationItem>
  );
  for (
    var i = Math.max(1, page - pagination_range);
    i <= max_page_in_range;
    i++
  ) {
    pagination_pages.push(PageLink(i));
  }

  return (
    <Pagination aria-label="Page navigation" className="flex gap-2">
      <PaginationContent>
        {page > 1 ? (
          <PaginationItem>
            <PaginationPrevious
              onClick={() => setPage((old: number) => old - 1)}
            />
          </PaginationItem>
        ) : (
          <> </>
        )}
        {page > 1 + pagination_range ? (
          <>
            <PaginationItem>
              <PaginationLink onClick={() => setPage(1)}>1</PaginationLink>
            </PaginationItem>
            <PaginationItem>
              <PaginationEllipsis />
            </PaginationItem>
          </>
        ) : (
          <> </>
        )}
        {pagination_pages}
        {max_page_in_range < lastPage ? (
          <>
            <PaginationItem>
              <PaginationEllipsis />
            </PaginationItem>
            <PaginationItem>
              <PaginationLink onClick={() => setPage(lastPage)}>
                {lastPage}
              </PaginationLink>
            </PaginationItem>
          </>
        ) : (
          <> </>
        )}

        {page < lastPage ? (
          <PaginationItem>
            <PaginationNext onClick={() => setPage((old: number) => old + 1)} />
          </PaginationItem>
        ) : (
          <> </>
        )}
      </PaginationContent>
    </Pagination>
  );
}
