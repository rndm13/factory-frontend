import {
  DialogContent,
  DialogTitle,
  DialogHeader,
  DialogClose,
  DialogFooter,
} from "@/components/ui/dialog";
import { Button } from "@/components/ui/button";

export function DeletionModal({ mutation }: { mutation: any }) {
  return (
    <DialogContent>
      <DialogHeader>
        <DialogTitle>Deletion confirmation</DialogTitle>
      </DialogHeader>
      {mutation.isPending ? (
        <p>Processing...</p>
      ) : mutation.isError ? (
        <>
          <div>An error occurred: {mutation.error.message}</div>
          <DialogFooter>
            <DialogClose asChild>
              <Button variant="outline" onClick={() => mutation.reset()}>
                Close
              </Button>
            </DialogClose>
          </DialogFooter>
        </>
      ) : mutation.isSuccess ? (
        <>
          <div>Successfully deleted</div>
          <DialogFooter>
            <DialogClose asChild>
              <Button variant="outline" onClick={() => mutation.reset()}>
                Close
              </Button>
            </DialogClose>
          </DialogFooter>
        </>
      ) : (
        <>
          <h1>Warning</h1>
          <p>You are about to delete a record. This action is irreversible</p>

          <>
            <DialogFooter>
              <DialogClose asChild>
                <Button variant="outline">Cancel</Button>
              </DialogClose>
              <Button onClick={() => mutation.mutate()}>Proceed</Button>
            </DialogFooter>
          </>
        </>
      )}
    </DialogContent>
  );
}
