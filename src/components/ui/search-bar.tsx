import { useState } from "react";
import { Input } from "./input";

export function SearchBar({ initialSearch, setKey }: { initialSearch: string; setKey: any }) {
  const [search, setSearch] = useState(initialSearch);

  return (
    <form
      className="w-full"
      onSubmit={(ev) => {
        ev.preventDefault();

        setKey(search);
      }}
    >
      <Input
        id="search"
        type="text"
        placeholder="Search..."
        value={search}
        onChange={(ev) => setSearch(ev.target.value)}
      />
      <input type="submit" className="hidden" />
    </form>
  );
}
