export function Header() {
  return (
    <header className="flex items-center h-16 px-4 border-b bg-transparent md:px-6">
      <nav className="hidden space-x-4 md:flex flex-1 min-w-0 items-center">
        <a className="font-medium" href="/customers">
          Customers
        </a>
        <a className="font-medium" href="/products">
          Products
        </a>
        <a className="font-medium" href="/orders">
          Orders
        </a>
      </nav>
    </header>
  )
}
