import { PaginationControls } from "@/components/ui/pagination-controls";
import { Button } from "@/components/ui/button";
import {
  CardTitle,
  CardDescription,
  CardHeader,
  CardContent,
  Card,
} from "@/components/ui/card";
import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";
import {
  TableHead,
  TableRow,
  TableHeader,
  TableCell,
  TableBody,
  Table,
} from "@/components/ui/table";
import { FileEditIcon, TrashIcon } from "@/components/ui/icons";

import { useState } from "react";
import { useQuery, useMutation, useQueryClient } from "@tanstack/react-query";

import {
  Dialog,
  DialogTrigger,
  DialogContent,
  DialogTitle,
  DialogHeader,
  DialogClose,
  DialogFooter,
} from "@/components/ui/dialog";
import axios from "axios";
import { DeletionModal } from "@/components/ui/deletion-modal";
import { SearchBar } from "./components/ui/search-bar";

export interface Product {
  id: number;
  category: string;
  model: string;
  price: number;
  in_stock: number;
  width: number;
  height: number;
  material: string;
  created_at: Date;
  updated_at: Date;
}

export class ProductOption {
  constructor(public product: Product) {}

  get value(): string {
    return `${this.product.id}`;
  }
  get label(): string {
    return `${this.product.model} | ${this.product.category} | ${this.product.price}`;
  }
  get color(): string {
    return "#ffffff";
  }
}

export function ProductModal({
  title,
  originalProduct,
  mutation,
}: {
  title: string;
  originalProduct: Product;
  mutation: any;
}) {
  const [product, setProduct] = useState<Product>(originalProduct);
  return (
    <DialogContent>
      <DialogHeader>
        <DialogTitle>{title}</DialogTitle>
      </DialogHeader>
      {mutation.isPending ? (
        <p>Processing...</p>
      ) : mutation.isError ? (
        <>
          <div>An error occurred: {mutation.error.message}</div>
          <DialogFooter>
            <DialogClose asChild>
              <Button variant="outline" onClick={() => mutation.reset()}>
                Close
              </Button>
            </DialogClose>
          </DialogFooter>
        </>
      ) : mutation.isSuccess ? (
        <>
          <div>Successfully saved!</div>
          <DialogFooter>
            <DialogClose asChild>
              <Button variant="outline" onClick={() => mutation.reset()}>
                Close
              </Button>
            </DialogClose>
          </DialogFooter>
        </>
      ) : (
        <>
          <div className="grid grid-cols-2 gap-4">
            <div className="space-y-2">
              <Label htmlFor="category">Category</Label>
              <Input
                id="category"
                placeholder="Window"
                required
                value={product.category}
                onChange={(value) =>
                  setProduct((old) => {
                    return { ...old, category: value.target.value };
                  })
                }
              />
            </div>
            <div className="space-y-2">
              <Label htmlFor="model">Model</Label>
              <Input
                id="model"
                placeholder="Big window"
                required
                value={product.model}
                onChange={(value) =>
                  setProduct((old) => {
                    return { ...old, model: value.target.value };
                  })
                }
              />
            </div>
          </div>
          <div className="grid grid-cols-2 gap-4">
            <div className="space-y-2">
              <Label htmlFor="width">Width</Label>
              <Input
                type="number"
                id="width"
                required
                value={product.width}
                onChange={(value) =>
                  setProduct((old) => {
                    return { ...old, width: +value.target.value };
                  })
                }
              />
            </div>
            <div className="space-y-2">
              <Label htmlFor="height">Height</Label>
              <Input
                type="number"
                id="height"
                required
                value={product.height}
                onChange={(value) =>
                  setProduct((old) => {
                    return { ...old, height: +value.target.value };
                  })
                }
              />
            </div>
          </div>
          <div className="grid grid-cols-2 gap-4">
            <div className="space-y-2">
              <Label htmlFor="price">Price</Label>
              <Input
                type="number"
                id="price"
                required
                value={product.price}
                onChange={(value) =>
                  setProduct((old) => {
                    return { ...old, price: +value.target.value };
                  })
                }
              />
            </div>
            <div className="space-y-2">
              <Label htmlFor="in-stock">In Stock</Label>
              <Input
                type="number"
                id="in-stock"
                required
                value={product.in_stock}
                onChange={(value) =>
                  setProduct((old) => {
                    return { ...old, in_stock: +value.target.value };
                  })
                }
              />
            </div>
          </div>
          <div className="space-y-2">
            <Label htmlFor="material">Material</Label>
            <Input
              id="material"
              placeholder="Glass"
              required
              value={product.material}
              onChange={(value) =>
                setProduct((old) => {
                  return { ...old, material: value.target.value };
                })
              }
            />
          </div>
          <DialogFooter>
            <DialogClose asChild>
              <Button variant="outline">Cancel</Button>
            </DialogClose>
            <Button onClick={() => mutation.mutate(product)}>Save</Button>
          </DialogFooter>
        </>
      )}
    </DialogContent>
  );
}

function ProductRow({ product, client }: { product: Product; client: any }) {
  const updateProduct = useMutation({
    mutationFn: (product: Product) => {
      return axios.put(
        "http://127.0.0.1:8000/api/product/" + product.id,
        product,
      );
    },
    onSuccess: () => {
      client.invalidateQueries({ queryKey: ["products"] });
    },
  });
  const deleteProduct = useMutation({
    mutationFn: () => {
      return axios.delete("http://127.0.0.1:8000/api/product/" + product.id);
    },
    onSuccess: () => {
      client.invalidateQueries({ queryKey: ["products"] });
    },
  });

  return (
    <TableRow>
      <TableCell className="font-medium">{product.id}</TableCell>
      <TableCell className="font-medium">{product.category}</TableCell>
      <TableCell className="font-medium">{product.model}</TableCell>
      <TableCell>${product.price}</TableCell>
      <TableCell>{product.in_stock}</TableCell>
      <TableCell>{product.width}</TableCell>
      <TableCell>{product.height}</TableCell>
      <TableCell>{product.material}</TableCell>
      <TableCell className="text-right">
        <Dialog>
          <DialogTrigger asChild>
            <Button className="h-full" size="icon" variant="ghost">
              <FileEditIcon className="h-4 w-4" />
              <span className="sr-only">Edit</span>
            </Button>
          </DialogTrigger>
          <ProductModal
            title="Update Product Record"
            mutation={updateProduct}
            originalProduct={product}
          />
        </Dialog>
        <Dialog>
          <DialogTrigger asChild>
            <Button className="h-full" size="icon" variant="ghost">
              <TrashIcon className="h-4 w-4" />
              <span className="sr-only">Delete</span>
            </Button>
          </DialogTrigger>
          <DeletionModal mutation={deleteProduct} />
        </Dialog>
      </TableCell>
    </TableRow>
  );
}

export function Products() {
  const [page, setPage] = useState(1);
  const [search, setSearch] = useState("");

  const queryClient = useQueryClient();

  const { isLoading, isError, error, data } = useQuery({
    queryKey: ["products", page, search],
    queryFn: async () => {
      const res = await fetch(
        `http://127.0.0.1:8000/api/products?page=${page}&search=${search}`,
      );
      return await res.json();
    },
  });

  const addProduct = useMutation({
    mutationFn: (newProduct: Product) => {
      return axios.post("http://127.0.0.1:8000/api/product", newProduct);
    },
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ["products"] });
    },
  });

  return (
    <div className="flex-1 min-h-0 w-full flex items-center justify-center p-4">
      <div className="grid w-full gap-4">
        <Card>
          <CardHeader>
            <CardTitle>Products</CardTitle>
            <CardDescription>Manage your products</CardDescription>
          </CardHeader>
          <CardContent>
            {isLoading ? (
              <h1 className="w-full text-center">Loading...</h1>
            ) : isError ? (
              <h1 className="w-full text-center">Error: {error.message}</h1>
            ) : (
              <div className="grid w-full gap-4">
                <div className="columns-3 items-center gap-4">
                  <Dialog>
                    <DialogTrigger asChild>
                      <Button size="sm">Add product</Button>
                    </DialogTrigger>
                    <ProductModal
                      title="Add Product"
                      originalProduct={{
                        id: 0,
                        category: "",
                        model: "",
                        price: 0,
                        in_stock: 0,
                        width: 0,
                        height: 0,
                        material: "",
                        created_at: new Date(),
                        updated_at: new Date(),
                      }}
                      mutation={addProduct}
                    />
                  </Dialog>
                  <PaginationControls
                    page={page}
                    setPage={setPage}
                    lastPage={data.meta.last_page}
                  />

                  <div className="ml-auto flex items-center gap-2">
                    <SearchBar initialSearch={search} setKey={setSearch} />
                  </div>
                </div>
                <Table>
                  <TableHeader>
                    <TableRow>
                      <TableHead className="w-[100px]">Product ID</TableHead>
                      <TableHead>Category</TableHead>
                      <TableHead>Model</TableHead>
                      <TableHead>Price</TableHead>
                      <TableHead>In Stock</TableHead>
                      <TableHead>Width</TableHead>
                      <TableHead>Height</TableHead>
                      <TableHead>Material</TableHead>
                      <TableHead className="w-[100px] text-center">
                        Actions
                      </TableHead>
                    </TableRow>
                  </TableHeader>
                  <TableBody>
                    {data.data.map((product: Product) => (
                      <ProductRow
                        client={queryClient}
                        product={product}
                        key={product.id}
                      />
                    ))}
                  </TableBody>
                </Table>
              </div>
            )}
          </CardContent>
        </Card>
      </div>
    </div>
  );
}
